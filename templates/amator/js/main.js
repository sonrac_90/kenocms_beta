/**
 * Created with JetBrains PhpStorm.
 * User: sonrac
 * Date: 30.08.13
 * Time: 15:28
 * To change this template use File | Settings | File Templates.
 */

/**
 * Длина коллекции obj
 * @param obj
 * @returns {number}
 */
    
// Подсчет количества элементов в коллекции
var len = function (obj){
    var _self = obj;
    var cnt = 0;
    if (typeof(_self) == 'object'){
        for(var i in _self){
            cnt++;
        }
    }
    return cnt;
}

/**
 * Обработчики навешиваются после загрузки страницы 
 */
$(document).ready(function (){
    // Change count day if change month
    var obj = $('[name^=month]');
    if ($(obj).size() > 0){
        $(obj).bind('change', function (e){
            e.preventDefault();
            var newmonthNum = parseInt($(this).val());
            var curDay = parseInt($('[name^=day]').val());
            var countDay = 31;
            if (newmonthNum === 2){
                countDay = 28;
            }else if (newmonthNum === 8){
            }else {
                if (newmonthNum > 7){
                    newmonthNum -= 1;
                }
                if (newmonthNum % 2 === 0)
                    countDay = 30;
            }
            
            if (curDay > countDay){
                curDay = 1;
            }
            
            var html = '';
            var sel = '';
            for (var i = 1; i <= countDay; i++){
                sel = '';
                if (i == curDay){
                    sel = 'selected=selected';
                }
                html += '<option value="' + i + '" ' + sel + '>' + i + '</option>\n';
            }
            return false;
        });
    }
    
    delete obj;
    
    obj = $('#result');
    // Навешиваю на каждую строку расчета переход к соответствующему расчету для просмотра рез-та
    if ($(obj).size() > 0){
        $('#result').find('tr').each(function (){
            $(this).on('click', function (event){
                event.preventDefault();
                var allTd = $(this).find('td');
                var dateTir = ($(allTd[0]).html() + '.' + $(allTd[1]).html());
                $.post('request.php', {'act' : 'getVar', 'name' : 'chCalc'}, function (data){
                    var dataSend = new Array();
                    dataSend = dateTir.split('.');
                    console.log(dataSend);
                    var chCalc = parseInt(data);
                    if (typeof chCalc !== 'number'){
                        chCalc = 0;
                    }
                    
                    $('#result').find('tr').each(function (){
                        $(this).remove();
                    })
                    
                    $.post('request.php',
                        {
                            act: 'getTemplates'
                        },
                        function (templates){
                            if (templates){
                                tr = $('<tr/>').append($('<td/>').html('<img width=128px height=128px src="templates/' + templates + '/images/load.gif" />'));
                                $(tr).css('cursor', 'hand');
                                $('#result').append($(tr));

                                var form = $('<form/>', {'action': 'index.php', 'method' : 'POST'})
                                    .append($('<input/>', {'type' : 'hidden', 'value' : 'goToCalcSet', 'name': 'act'}))
                                    .append($('<input/>', {'type' : 'hidden', 'value' : parseInt(data), 'name': 'chCalc'}))
                                    .append($('<input/>', {'type' : 'hidden', 'value' : dataSend[1], 'name': 'day'}))
                                    .append($('<input/>', {'type' : 'hidden', 'value' : dataSend[2], 'name': 'month'}))
                                    .append($('<input/>', {'type' : 'hidden', 'value' : dataSend[3], 'name': 'year'}))
                                    .append($('<input/>', {'type' : 'hidden', 'value' : dataSend[0], 'name': 'numTir'}));
                                $(form).submit();
                            }
                        }
                    );
                })
                return false;
            });
        });
    }
    
    delete obj;
    
    // Вернуться на страницу с квадратами
    $('#backToSquare').bind('click', function (event){
        event.preventDefault();
        var form = $('<form/>', {'action': 'index.php', 'method' : 'POST'})
            .append($('<input/>', {'type' : 'hidden', 'value' : 'deleteVarBack', 'name': 'act'}))
            .append($('<input/>', {'type' : 'hidden', 'value' : ', year', 'name': 'show'}))
        $(form).submit();
        return false;
    });
    
    // Возвратиться на главную
    obj = $('.backToMain');
    if ($(obj).size() > 0){
        $(obj).bind('click', function (event) {
            // Очищаю все переменные сессии
            $.post(
                'request.php',
                {
                    'act' : 'deleteVarBack'
                },
                function (data){
                    if (!data)
                        window.location.reload();
                    else
                        alert('Ошибка: ' + data);
                }
            );
            return false;
        });
    }
    
    // Навешивание обработчиков на кнопки
    delete obj;
    obj = $('button');
    if ($(obj).size() === 0){
        obj=$('input[type^=button]');
    }
    if ($(obj).size() > 0){
        $(obj).each(function (){
            if ($(this).html().indexOf('1')>0 || $(this).val().indexOf('1') > 0){
                $(this).bind('click', function (event){
                    var nameVar = 'numSquare';
                    if ($(this).hasClass('prir')){ // Приращение квадрата
                        nameVar = 'numAddition';
                    }else if ($(this).hasClass('positionChange')){ // Изменение номера позиции
                        nameVar = 'numPosition';
                    }else if ($(this).hasClass('positionSquareChange')){ // Изменение квдрата в статистике
                        nameVar = 'numSquarePosition';
                    }
                    var valPlus = $(this).html().replace(/&nbsp;/g, ''); // Величина приращения
                    if (!valPlus)
                        valPlus = $(this).val().replace(/&nbsp;/g, '');

                    $('<form/>', {action: 'index.php', method:'POST'})
                        .append($('<input/>', {type : 'hidden', value: 'changeSquareNumber', name: 'act'}))
                        .append($('<input/>', {type : 'hidden', value: nameVar, name: 'varChange'}))
                        .append($('<input/>', {type : 'hidden', value: parseInt(valPlus), name: 'valuePlus'}))
                        .submit();
                    return false;
                });
            }
        });
    }
    
    // Навешивание обработчика на кнопку рассчитать по ранее выбранным позициям
    $('#selectedPosition').bind('click', function (event){
        event.preventDefault();
        // Проверяю есть ли необходимые данные
        $.post(
            'request.php',
            {
                act: 'checkIsSetPosition'
            },
            function (data){
                if (data){
                    alert('Позиций не выбрано. Сначала выберите позиции');
                }else{
                    $.post('request.php',
                        {
                            act: 'getTemplates'
                        },
                        function (templates){
                            if (templates){
                                var load = $('<div/>').html('<img width=128px height=128px src="templates/' + templates + '/images/load.gif" />');
                                $('#squareResult').html($(load).html());

                                $('<form/>', {action:'index.php', method:'post'})
                                    .append($('<input/>', {'name': 'act', 'value': 'calculateStat', 'type':'hidden'}))
                                    .submit();
                            }
                        }
                    );
                }
            }
        )
        return false;
    })
    
    // Навешинание обработчика на кнопку "Центральная линия"
    $('#selectedCentral').bind('click', function (event){
        event.preventDefault();
        $.post('request.php',
            {
                act: 'getTemplates'
            },
            function (templates){
                if (templates){
                    console.log(templates);
                    var load = $('<div/>').html('<img width=128px height=128px src="templates/' + templates + '/images/load.gif" />');
                    $('#squareResult').html($(load).html());
                    $('<form/>', {action:'index.php', method:'post'})
                        .append($('<input/>', {'name': 'act', 'value': 'calculateStat', 'type':'hidden'}))
                        .append($('<input/>', {'name': 'centralLine', 'value': 'centralLine', 'type':'hidden'}))
                        .append($('<input/>', {'name': 'position', 'value': 'centralLine', 'type':'hidden'}))
                        .submit();
                }
            }
        );
        return false;
    });
    
    // Показать форму добавления позиций
    $('#selectPosition').bind('click', function (event){
        event.preventDefault();
        addPosition.showModalWindow();
        return false;
    });
    
    // Привязываю обработчики на нажатие по строке таблицы для перехода к расчету
    $('.tablResultStat').find('tr').each(function (){
        var td = $(this).find('td');
        if (parseInt($(td[0]).html())){
            $(this).addClass('cursPointer');
            $(this).bind('click', function (){
                var td = $(this).find('td');
                var countSovp = parseInt($(td[0]).html());
                $('<form/>', {action: 'index.php', method : 'POST'})
                    .append($('<input/>', {type: 'hidden', name: 'act', value: 'changeCurrentSovpPosition'}))
                    .append($('<input/>', {type: 'hidden', name: 'curSovp', value: countSovp}))
                    .submit();
            });
        }else if ($(td).size() == 2){
            $(this).addClass('cursPointer');
            $(this).bind('click', function (){
                $('<form/>', {action: 'index.php', method : 'POST'})
                    .append($('<input/>', {type: 'hidden', name: 'act', value: 'changeCurrentSovpPosition'}))
                    .submit();
            })
        }
    });
    
    // Выбор метода расчета квадрата
    $('select[name^=changeSqMethod]').bind('change', function (event){
        event.preventDefault();
        $.post(
            'request.php', 
            {
                'act': 'changeSqMethod',
                'newValue': $(this).val()
            },
            function (data){
                if (data){
                    alert(data);
                }else{
                    window.location.reload();
                }
            });
        return false;
    });
    
    // Обработчик на значке информации о расчете
    $('.info').each(function (){
        $(this).bind('click', function (event){
            var data = {};
            if ($(this).parent().is('td')){
                var trParent = $(this).parent().parent().find('td');
                var numberCalc = $(trParent[0]).html();
                data = {
                    act: 'getInfoCalculate',
                    numberCalculate : parseInt(numberCalc/3)
                }
            }else{
                data = {
                    act: 'getInfoCalculate'
                }
            }
            event.preventDefault();
            $.post(
                'request.php',
                data,
                function (data){
                    if (data){
                        $('#modal').html(data);
                        $('#modal').dialog({
                            modal: true,
                            draggable: true,
                            position : 'center',
                            modal: true,
                            closeOnEscape: true,
                            resizable: true,
                            title: 'Информация про расчет',
                            width: window.innerWidth - 100,
                            height:window.innerHeight - 100
                        });
                    }
                });
            return false;
        });
    });
    
    $('.showCalc').find('tr').each(function (){
        var tdTr = $(this).find('td');
        var numberCalc = parseInt($(tdTr[0]).html());
        if (!isNaN(numberCalc)){
            $(this).addClass('cursPointer');
            $(this).find('td').each(function (){
                if ($(this).find('div[class^=info]').size() === 0){
                    $(this).bind('click', function (event){
                        event.preventDefault()
                        
                        $.post(
                            'request.php',
                            {
                                act: 'deleteVarBack',
                                numberCalculate: numberCalc
                            },
                            function (data){
                                if (data){
                                    alert(data);
                                }else{
                                    $('<form/>', {action: 'index.php', method : 'POST'})
                                        .submit();
                                }
                            }
                        );
                        return false;
                    });
                }
            });
        }
    });
    
    // Кнопка перехода к выбору расчета
    $('#choiseCalc').bind('click', function (){
        // Получаю описание
        $.post(
            "request.php",
            {
                act : 'getShortDescription'
            },
            function (data){
                if (data){
                    $('#modal').html(data);
                    $('#modal').dialog({
                        modal: true,
                        draggable: true,
                        position : 'center',
                        modal: true,
                        closeOnEscape: false,
                        resizable: true,
                        title: 'Выбор тиража',
                        width: 1000,
                        height: 600,
                        resize: false,
                        closeOnEscape: true
                    });
                }
            }
        );
        // Переход к странице выбора расчета
//        $('<form/>', {action: 'index.php', method : 'POST'})
//            .append($('<input/>', {type: 'hidden', name: 'act', value: 'deleteVarBack'}))
//            .append($('<input/>', {type: 'hidden', name: 'choiseCalc', value: 'choiseCalc'}))
//            .submit();
//    });
        
    });
    
    $('#changeCalculate').bind('change', function (event){
        event.stopPropagation();
        event.preventDefault();
        $.post(
            'request.php',
            {
                act : 'setNewCalc',
                value : $(this).val(),
                num : parseInt($(this).val() / 3 + 1)
            },
            function (data){
                if (data) {
                    alert('Ошибка ' + error);
                }else{
                    console.log($(this).find('option'));
                    window.location.reload();
                }
            }
        );
        
        return false;
    });
    
//    $('body').height(window.innerHeight)
});

/**
 * Объект, инкапсулирующий свойства и методы добавления позиций
 * @type {
 * {cntPosition: number,
 * position: {},
 * colorPosition: {1: string, 2: string, 3: string, 4: string, 5: string, 6: string, 7: string, 8: string, 9: string, 10: string}, 
 * showModalWindow: Function, 
 * deleteAddPosition: Function}}
 */
var addPosition = {

    cntPosition : 1,
    position: {},
    colorPosition: {
        1 : 'choise1',
        2 : 'choise2',
        3 : 'choise3',
        4 : 'choise4',
        5 : 'choise5',
        6 : 'choise6',
        7 : 'choise7',
        8 : 'choise8',
        9 : 'choise9',
        10 : 'choise10'
    },
    
    // Показываю форму выбора позиций (диалоговое окно)
    showModalWindow : function(){
        $('#modal').html('');
        $('#modal').attr({'align': 'center'});
        var square = '';
        $('.resultSquare').each(function (){
            square = $(this).clone();
        });
        
        var countTd = 0;
        
        $(square).find('td').each(function (){
            countTd++;
            $(this).attr('id', 'num' + countTd);
            $(this).addClass('cursPointer');
            $(this).bind('click', function (event){
                var num = parseInt($(this).attr('id').replace(/num/g, ''));
                if (isNaN(parseInt($(this).html()))){
                    return false;
                }
                var lengthPos = 0;
                if (addPosition.position !== undefined){
                    var a = addPosition.position[addPosition.cntPosition];
                    for (var v in a){
                        lengthPos++;
                    }
                }else{
                    addPosition.position = {};
                }
                if (!$(this).attr('choise') ||
                    $(this).attr('choise').indexOf(addPosition.cntPosition) === -1){
                    var choiseOld = $(this).attr('choise'); 
                    $(this).attr('choise', choiseOld + ' ' + addPosition.cntPosition);
                    if (addPosition.position[addPosition.cntPosition] !== undefined){
                        addPosition.deleteAddPosition(true, num);
                    }else{
                        addPosition.position[addPosition.cntPosition] = {};
                        addPosition.position[addPosition.cntPosition][1] = num;
                    }
                    $(this).addClass(addPosition.colorPosition[addPosition.cntPosition]);
                }else{ // Удaляю выбор
                    // В этой позиции число еще не выбиралось
                    if ($(this).hasClass(addPosition.colorPosition[addPosition.cntPosition])){
                        // Удаляю класс
                        $(this).removeClass(addPosition.colorPosition[addPosition.cntPosition]);
                        console.log($(this).attr('class'));
                        var clName = $(this).attr('class');
                        this.className = clName;
                        console.log(this.className);
                        // Удаляю ячейку из позиции
                        addPosition.deleteAddPosition(false, num, lengthPos);
                    }else{ // Просто добавляю класс
                        $(this).addClass(addPosition.colorPosition[addPosition.cntPosition]);
                        // Добавляю ячейку в позицию
                        addPosition.deleteAddPosition(true, num);
                    }
                }
            });
        });

        $('#modal').css('display', 'none');
        $('#modal').append('<div/>', {'class': 'resultModal'});
        var a = $('#modal').find('div');
        $(a).append($(square));
        var w = $('#modal').width() + 180;
        if (w < 542) w=500;
        var h = $('#modal').height() + 160;
        if (h > 542) h = 542;

        // Индикаторы управления позициями
        var div = $('<div/>', {class: 'positionNumber'});
        
        for (var i = 1; i < 11; i++){
            var div1 = $('<div/>', {'id': 'pos' + i, 'class' : 'positionNumberOne'})
                .html('&nbsp;&nbsp;' + i + '&nbsp;&nbsp;');
            $(div).append($(div1));
        }

        // Выбор позиции для ввода
        $(div).find('div').each(function (){
            $(this).bind('click', function (event){
                event.preventDefault();
                var numPos = parseInt($(this).attr('id').replace(/pos/g, ''));
                if (addPosition.position[numPos] !== undefined || numPos === 1){
                    addPosition.cntPosition = numPos;
                    $('#currentPosition').html(addPosition.cntPosition);
                }else{
                    alert('Данная позиция не заполнена');
                }
                return false;
            })
        });
        
        $('#modal').append($(div));
        
        var butt = $('<input/>', {type : 'button', value: 'Очистить выбор'}).bind('click', function (event){
            delete addPosition.position;
            addPosition.cntPosition = 1;
            $('#modal').dialog('close');
            return false;
        });
        $('#modal').prepend($(butt));
        butt = $('<input/>', {type: 'button', value: 'Добавить еще одну...'}).bind('click', function (event){
            var cnt = 0;
            for (var i in addPosition.position[addPosition.cntPosition]){
                cnt++;
            }
            if (addPosition.position[addPosition.cntPosition] !== undefined && cnt >= 2){
                
                $('#pos' + addPosition.cntPosition).addClass(addPosition.colorPosition[addPosition.cntPosition]);
                addPosition.cntPosition = parseInt(len(addPosition.position));
                addPosition.cntPosition++;
                addPosition.cntPosition = (addPosition.cntPosition > 10) ? (10) : addPosition.cntPosition;
                //if ()
                $('#currentPosition').html(addPosition.cntPosition);
            }else{
                alert('Введите правильно позицию (цифр должно быть не менее 2!!!)');
            }
            return false;
        });
        $('#modal').prepend($(butt));
        
        butt = $('<input/>', {type: 'button', value: 'Рассчитать...'}).bind('click', function(event){
            if (addPosition.position[1] !== undefined 
                && (len(addPosition.position[1]) > 1)){
                // Загоняю позицию в базу и слежу чтобы все корректно отобразилось
                var position = JSON.stringify(addPosition.position);
                $.post(
                    'request.php',
                    {
                        act : 'calculateStat',
                        position: '\'' + position + '\''
                    },
                    function (data){
                        if (data){// Возникла ошибка, необходимо удалить из сессии флаг показа статистики
                            $.post(
                                'request.php',
                                {
                                    act: 'deleteVar',
                                    name : 'statistics'
                                }
                            )
                            alert('Невозможно сохранить позицию :' + "\n" + data);
                        }else{ // Все прошло успешно
                            // Вешаю индикатор загрузки
                            $.post('request.php',
                                {
                                    act: 'getTemplates'
                                },
                                function (templates){
                                    if (templates){
                                        var load = $('<div/>').html('<img width=128px height=128px src="templates/' + templates + '/images/load.gif" />');
                                        $('#modal').html($(load).html());

                                        var form = $('<form/>', {method: 'POST', action: 'index.php'})
                                            .append($('<input/>', {type: 'hidden', name: 'act', value: 'calculateStat'}))
                                            .append($('<input/>', {type: 'text', name: 'position', value: position}))
                                        $(form).submit();
                                    }
                                }
                            );
                        }
                    }
                );
            }
            return false;
        });
        $('#modal').prepend($(butt));
        $('#modal').append($('<div/>').html('Сейчас активна позиция №: <div id=currentPosition style="display:inline-block;" >' +  addPosition.cntPosition + '</div>'));
        $('#modal').dialog({
            modal: true,
            draggable: true,
            position : 'center',
            modal: true,
            closeOnEscape: false,
            resizable: true,
            title: 'Выбор тиража',
            width: w,
            height: h
        });
    },
    
    deleteAddPosition: function (delOrAdd, num){
        var cnt = 0;
        var tmp = {};
        var is=false;
        var a = addPosition.position[addPosition.cntPosition];
        for (var s in a){
            cnt++;
            if (delOrAdd){
                if (a[s] > num && !is){
                    tmp[cnt] = num;
                    cnt++;
                    is = true;
                }
                tmp[cnt] = a[s];
            }else{
                if (a[s] != num){
                    tmp[cnt] = a[s];
                }else{
                    cnt--;
                }
            }
        }
        if (!is && delOrAdd){
            tmp[cnt + 1] = num;
        }
        delete addPosition.position[addPosition.cntPosition];
        addPosition.position[addPosition.cntPosition] = tmp;

        return is;
    },
    
    findNextPosition : function (){
        for (var i = 9; i >= 0; i--){
            console.log(i);
            var className = $('#pos' + parseInt(i + 1)).attr('class');
            var match = className.match(/choise/g); 
            if (typeof(addPosition.cntPosition[i]) !== 'undefined' &&
                typeof(match) !== 'undefined'){
                return parseInt(i+1);
            }else{
                console.log(addPosition.cntPosition[i]);
//                console.log($('#pos' + parseInt(i + 1)).attr('class').match(/choise/g).length);
            }
        }
    }
    
}
