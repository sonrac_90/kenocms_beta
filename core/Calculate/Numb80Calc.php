<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 12.09.13
 * Time: 10:09
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class KenoCMS_Numb80Retry
 * Данный класс работает с расчетом, где в качестве квдрата выступают цифры от 1 до 80
 * строятся они конечно по-разному, но смысл от этого не меняется!!!
 * Поэтому все лежит в одном классе, а наследуется от базового calculate
 */

class KenoCMS_Numb80Retry extends KenoCMS_calculate{

    /**
     * Дополнение к описанному ранее конструктору
     */
    public function additional(){
        $sess = $_SESSION;
        $this->date = $sess['day'] . "." . $sess['month'] . "." . $sess['year'];
        // Получаю Базовую строку
        $this->baseArr = $this->db->GetResultArr($sess['day'], $sess['month'], $sess['year']);
        // Получаю массив поиска
        $this->searchArr = $this->db->GetResultArr($sess['day'], $sess['month'], intval($sess['year'] + 1), TBLPREFIX . 'result_sort');
        
        // Таблицы результатов
        $this->tblName['squareSave'] = TBLPREFIX . 'calcresult_number80';
        $this->tblName['position'] = TBLPREFIX . 'position';
        $this->tblName['positionPriceSave'] = TBLPREFIX . 'positionResultSquare';
        $this->tblName['positionSquareSave'] = TBLPREFIX . 'positionResultSquare';
    }

    /**
     * Проверяю, есть ли в массиве $arr значение $srch
     * @param $arr
     * @param $srch
     * @return bool - true - если есть
     */
    public function isReplay($arr, $srch){
        for ($i = 0; $i < count($arr); $i++){
            for ($j = 0; $j < count($arr[$i]); $j++){
                if ($arr[$i][$j] == $srch){
                    return true;
                }
            }
        }
        
        return false;
    }

    /**
     * Рассчитываю 1 строку, из которой формируется квадрат
     * @param string $methodBuild - метод построения 
     * (one - базовая строка не участвует, числа записываются подряд от 1 до 80;
     *  horizontal - дополнением формируется 20 строк, потом выбирается по горизонтали числа по очереди
     *  vertical -  дополнением формируется 20 строк, потом выбирается по вертикали числа по очереди
     *  diapazon - числа заполняются диапазонно, если число четное - то под ним только четные и наоборот)
     * @param bool $one - возвращать 2мерный или исходный массив
     * @param string $methodLastBuild - метода построения строки (вертикаль/горизонталь)
     * @return array - двумерный заполненный массив
     */
    public function BuildFirstLine($methodBuild='horizontal', $methodLastBuild='horizontal', $one=false){
        $resBuild = array();
        if ($methodBuild === 'one'){
            $cnti = 0;
            $cntj = 0;
            for ($i = 0; $i < 80; $i++){
                $resBuild[$cnti][$cntj] = intval($i + 1);
                $cntj++;
                if ($cntj > 19){
                    $cntj = 0;
                    $cnti++;
                }
            }
        }else{
            $additionNumber = 1;
            if ($methodBuild === 'diapazon'){
                $additionNumber = 2;
            }
            if ($methodLastBuild === 'horizontal'){
                $resBuild[0] = $this->baseArr;
            }else{
                for ($i = 0; $i < 20; $i++){
                    $resBuild[$i][0] = $this->baseArr[$i];
                }
            }
            for ($i = 0; $i < 20; $i++){
                $n = 1;
                $j = 0;
                $setter = false;
                do {
                    $j++;
                    if ($methodLastBuild === 'horizontal'){
                        if (isset($resBuild[$j - 1][$i])){
                            $n = $this->sum($resBuild[$j - 1][$i], $additionNumber);
                            if (!$this->isReplay($resBuild, $n)){
                                $resBuild[$j][$i] = $n;
                            }
                        }
                        $setter = isset($resBuild[$j][$i]);
                    }else{
                        if (isset($resBuild[$i][$j - 1])){
                            $n = $this->sum($resBuild[$i][$j - 1], $additionNumber);
                            if (!$this->isReplay($resBuild, $n)){
                                $resBuild[$i][$j] = $n;
                            }
                        }
                        $setter = isset($resBuild[$i][$j]);
                    }
                }while(!$this->isReplay($resBuild, intval($this->sum($n, $additionNumber))) && $setter);
            }
        }
        if ($one){
            return $resBuild;
        }else{
            return $this->buildArrayFromArr($this->retOneArrFromTwo($resBuild), 9, 9, 0);
        }
    }

    /**
     * Вращение квадрата путем помещения последней цифры на первую позицию, а все остальные смещаются на 1 позицию вперед
     * @param array $fLine- исходный массив
     * @param number $interval - Какой квадрат по счету
     * @return array - результат смещения
     */
    public function RotateSquare ($fLine=array(), $interval=0){
        $newArr = array();
        
        for ($i = $interval; $i < count($fLine); $i++){
            $newArr[] = $fLine[$i];
        }
        
        for ($i = 0; $i < $interval; $i++){
            $newArr[] = $fLine[$i];
        }

        return $newArr;
    }

    /**
     * Заполнение чисел через интервал (к примеру строка 1 2 3 4 5 6 
     * с интервалом 2 на 1 итерации:
     * 1 ч ч 2 ч  ч
     * на 2 итерации
     * 1 3 ч 2 4 ч
     * на 3 итерации
     * 1 3 5 2 4 6
     * @param $fline - исходный одномерный БМ
     * @param $interval - интервал цифр
     */
    public function IntervalSquare ($fline, $interval){
        $newArr = array();
        $cnt = 0;
        $lengthArr = count($fline);
        $interval++;
        $cntStop = 0;
        for ($i = 0; $i < $lengthArr; $i++){
            $newArr[$cnt] = $fline[$i];
            $cnt += $interval;
            if ($cnt > ($lengthArr - 1)){
                $cntStop++;
                $cnt = $cntStop;
            }
        }
        
        return $newArr;
    }
    
    /**
     * Подсчет:
     * 1. Строится строка должным образом
     * 2. Со строкой выполняются операции
     * 3. Строится квадрат должным образом
     * 4. Приращение
     * @param string $methodSrchLine - Как строится базовая строка (one, horizontal, vertical)
     * @param number $methodStr - Метод, по которому строится квадрат(0 - строка в строку, 1 - спираль, 2 - диагональ)
     * @param string $methodPrepSq - Метод построения строки перед построением квадрата (interval, rotate, intRotate, null)
     * @param string $methodPlus - Метод сложения (plus, minus, plMinus)
     * @param bool $addition - Стоит ли делать приращение на 80
     * @param string $methodBuildFirstStrTwo - Метод вторичной постройки базовой строки (числа в квадрат записываются либо по горизонтали либо по вертикали)
     */
    public function calculate (
                                $methodSrchLine='one',
                                $methodStr=null,
                                $methodPrepSq=null,
                                $addition=true,
                                $methodPlus = 'plus',
                                $methodBuildFirstStrTwo='horizontal'
                              ){
        // Если в базе имеется такой расчет, то рассчитывать уже не нужно
        if ($this->db->GetIsOrNotCalcDateInDb($this->tblName['squareSave'], $this->date)){
            return;
        }
        
        $this->squareSave = '';
        $this->positionPriceSave = '';
        $this->positionSquareSave = '';
        
        $addToSql = 'null, \'' . $this->date . '\', ' . $_SESSION['chCalc'];
        
        // Базовая строка, из которой строятся все остальные квадраты и строки
        $firstLine = $this->retOneArrFromTwo($this->BuildFirstLine($methodSrchLine, $methodBuildFirstStrTwo), 20);
        
        // Первый квадрат
        $firstSq = array();
        
        $maxRot = @substr_count('interval rotate intRotate', $methodPrepSq) ? $this->maxRotate : $this->maxAddition;
        for ($interval = 0; $interval < $maxRot; $interval++){
            $prir = true;
            switch ($methodPrepSq){
                case 'interval' :
                    $prepline = $this->IntervalSquare($firstLine, $interval);
                    break;
                case 'rotate' :
                    $prepline = $this->RotateSquare($firstLine, $interval);
                    break;
                case 'intRotate' :
                    $prepline = $this->IntervalSquare($this->RotateSquare($firstLine, $interval), $interval);
                    break;
                default:
                    $prir = false;
                    $prepline = $firstLine;
                    break;
            }
            if ($prir && $addition){
                $firstSq = $this->buildArrayFromArr($prepline, 9, 9, $methodStr);

                $this->constructStringSql('squareSave', '(' . $addToSql . ', ' . $interval . ', 0' . $this->tmpSql . ')');
                for ($j = 1; $j < $this->maxAddition; $j++){
                    $this->addPrirToSquare($firstSq, $j);
                    $this->constructStringSql('squareSave', ', (' . $addToSql . ', ' . $interval . ', ' . $j . $this->tmpSql . ')');
                }
            }else{
                if ($interval === 0){
                    $firstSq = $this->buildArrayFromArr($prepline, 9, 9, $methodStr);
                    $this->constructStringSql('squareSave', '(' . $addToSql . ', 0, ' . $interval . $this->tmpSql . ')');
                }else{
                    $addString = ', 0, ' . $interval;
                    if ($addition)
                        $this->addPrirToSquare($firstSq, $interval);
                    else{
                        $this->buildArrayFromArr($prepline, 9, 9, $methodStr);
                        $addString = ', ' . $interval . ', 0';
                    }
                    $this->constructStringSql('squareSave', ', (' . $addToSql . $addString . $this->tmpSql . ')');
                }
            }
        }
        // Добавляю те строки, которые не добавились в БД во время расчета
        $this->constructStringSql('squareSave', '', true);
    }
    
    /**
     * Вывод квадрата на экран
     * @param $arr  - двумерный массив
     */
    public function PrintTable($arr=null, $start=null){
        if ($arr){
            $this->pt->PrintSquare9x9($arr, $this->searchArr, 2);
        }else{
            $what = 'num1, num2, num3, num4, num5, num6, num7, num8, num9, num10, num11, num12, num13, num14, num15, num16, num17, num18, num19, num20, num21, num22, num23, num24, num25, num26, num27, num28, num29, num30, num31, num32, num33, num34, num35, num36, num37, num38, num39, num40, num41, num42, num43, num44, num45, num46, num47, num48, num49, num50, num51, num52, num53, num54, num55, num56, num57, num58, num59, num60, num61, num62, num63, num64, num65, num66, num67, num68, num69, num70, num71, num72, num73, num74, num75, num76, num77, num78, num79, num80';
            $arr = $this->db->GetIsOrNotCalcDateInDb($this->tblName['squareSave'], $this->date, false, array(intval($_SESSION['numSquare']), intval($_SESSION['numAddition']), $what));
            $this->pt->PrintSquare9x9($arr, $this->searchArr);
        }
    }
    
    public function __destruct(){}
}

?>
