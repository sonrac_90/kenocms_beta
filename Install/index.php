<!DOCTYPE>
<html>
<head>
    <title>
        Установка KenoCMS "Игра по системе"
    </title>
    <meta charset='utf-8' />
    <script src="../templates/default__/js/jquery.js" type=text/javascript></script>
    <script src=js/main.js type="text/javascript"></script>
</head>
<body>
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey
 * Team: SoNRaC TeaM
 * Date: 15.09.13
 * Time: 23:41
 * To change this template use File | Settings | File Templates.
 */
if (!isset($_POST['act'])){
    echo "Тут может быть приветствие";
    ?>
    <br/>
    <?php
    $filename = '../config/config.db.php';
    if (is_file($filename)){
        ?>
        Файл конфигурации существует!<br/>
        <a href="../" >Нажмите, чтобы вернуться на главную </a><br/>
        Нажмите кнопку снизу, чтобы перезаписать файл, если это действительно нужно<br/>
        <?php
    }else{
    ?>
    Для начала необходимо настроить базу данных для работы<br/>
        <?php
    }?>
        <form action="index.php" method="POST">
            <input type=hidden value=configDb name=act />
            <input type=submit value="Перейти к настройке БД"/>
        </form>
    <?php

    }else{
    switch ($_POST['act']){
        case 'configDb' :
            ?>
            <form action="index.php" method="post">
                <input type=hidden value=saveConfig name=act />
                Имя хоста: <input type=text value="localhost" name=host /><br/>
                Имя пользователя: <input type=text value="root" name=user /><br/>
                Пароль пользователя: <input type=password value="password" name=pass id=pass />
                <input type="checkbox" name=show>Показать пароль<br/>
                Имя базы данных: <input type=text value="keno1" name=namedb /><br/>
                Префикс для таблиц: <input type=text value="keno_" name=tblpref /><br/>
                <input type="submit" value="Заполнить БД" />
            </form>
            
<?php
            break;
        case 'saveConfig': 
            $fileTextConf = "<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 15.09.13
 * Time: 22:57
 * To change this template use File | Settings | File Templates.
 */

/**
 * Хост БД
 */
define('HOST', '" . $_POST['host'] . "');

/**
 * Пароль
 */
define('PASS', '" . $_POST['pass'] . "');

/**
 * Пользователь
 */
define('USER', '" . $_POST['user'] . "');

/**
 * Префикс таблиц
 */
define('TBLPREFIX', '" . $_POST['tblpref'] . "');

/**
 * Имя БД
 */
define('DBNAME', '" . $_POST['namedb'] . "');
?>
";
            $filename = '../config/config.db.php';
            $file = fopen($filename, 'w');
            if ($file){
                fwrite($file, $fileTextConf);
                fclose($file);
                // Запрещаю запись для всех кроме владельца!
                chmod($filename, '101755');
                echo 'Файл конфигурации создан успешно';
            }
            else{ 
                ?>
                <div align=center>
                <pre>
                Нет разрешения на запись в директории!!!
                Cоздайте сами в директории <?php echo $_SERVER['DOCUMENT_ROOT'] . str_replace('Install/index.php', '', $_SERVER['SCRIPT_NAME']);?>
                    
                Cкрипт 'config.db.php'. Следующего содержания:
                </pre>
                <div align=left style="background-color: #a3a655; width: 600px;"><pre>
                <code>
                    <?php echo htmlspecialchars($fileTextConf); ?>
                </code></pre>
                </div>
                    <pre>
                
                Либо выставьте права на запись для всех для директории (в Linux следующим образом):
                $ sudo chmod -R 0777 <?php echo $_SERVER['DOCUMENT_ROOT'] . str_replace('Install/index.php', '', $_SERVER['SCRIPT_NAME']);?>
                В противном случае система функционировать не будет по причине отсутствия корректных натсроек для подключения к БД.
                    </pre></div>

<?php
            }
            echo '<br/><br/><a href="../?numCalc=0">Вернуться на главную</a><br/>';
            echo '<a href="index.php">Перезапустить инсталлятор</a>';
            break;

    }
}

?>

</body>
</html>
