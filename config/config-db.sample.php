<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 15.09.13
 * Time: 22:57
 * To change this template use File | Settings | File Templates.
 */

/**
 * Хост БД
 */
define('HOST', 'хост для подключния к БД Mysql (локально - localhost)');

/**
 * Пароль
 */
define('PASS', 'пароль пользователя');

/**
 * Пользователь
 */
define('USER', 'имя пользователя');

/**
 * Префикс таблиц
 */
define('TBLPREFIX', 'префикс таблиц, если нет - то пусто');

/**
 * Имя БД
 */
define('DBNAME', 'имя базы данных');
?>
